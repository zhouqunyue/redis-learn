package com.hmdp.interceptor;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.hmdp.dto.UserDTO;
import com.hmdp.utils.RedisConstants;
import com.hmdp.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 当用户使用不需要拦截的内容时 一旦超过30分钟 token就会失效 所有需要一个拦截器拦截所有请求并且刷新token
 */
@Slf4j
public class RefreshTokenInterceptor implements HandlerInterceptor {

    //拦截器是直接new出来的对象 所以不能自动注入  要通过构造函数导入
    private StringRedisTemplate redisTemplate;

    public RefreshTokenInterceptor(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //TODO 1 获取请求头中的token
        String token = request.getHeader("authorization");

        if (StrUtil.isBlank(token)) {//Blank空白
            return true;
        }

        //2 基于token获取redis中的用户  返回map里面的所有信息     entry方法会自动判断null 如果为null 会返回一个空map
        Map<Object, Object> userMap = this.redisTemplate.opsForHash().entries(RedisConstants.LOGIN_USER_KEY + token);
        //3 判断用户是否存在
        if (userMap.isEmpty()) {
            return true;
        }

        //将查到的Hash数据转为UserDTO对象
        UserDTO userDTO = BeanUtil.fillBeanWithMap(userMap, new UserDTO(), false);

        log.info(userDTO.toString());
        //5 保存用户信息到TreadLocal
        UserHolder.saveUser(userDTO);
        //6 刷新token有效期    expire 失效
        this.redisTemplate.expire(RedisConstants.LOGIN_USER_KEY + token, RedisConstants.LOGIN_USER_TTL, TimeUnit.MINUTES);
        //7 放行
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        //移除用户 避免内存泄露
        UserHolder.removeUser();
    }
}
