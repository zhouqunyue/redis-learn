package com.hmdp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.dto.LoginFormDTO;
import com.hmdp.dto.Result;
import com.hmdp.dto.UserDTO;
import com.hmdp.entity.User;
import com.hmdp.mapper.UserMapper;
import com.hmdp.service.IUserService;
import com.hmdp.utils.RedisConstants;
import com.hmdp.utils.RegexUtils;
import com.hmdp.utils.SystemConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {


    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private UserMapper userMapper;

    /**
     * 完成手机验证码的发送
     *
     * @param phone   手机号码
     * @param session session
     * @return
     */
    @Override
    public Result sendCode(String phone, HttpSession session) {
        //1 校验手机号
        if (RegexUtils.isPhoneInvalid(phone)) {
            //校验不通过
            //2 不符合 返回不符合信息
            return Result.fail("手机号错误");
        }

        //3 符合 生成验证码
        String code = RandomUtil.randomNumbers(6);
        //4  将验证码保存到redis   加上login:code前缀  分清各种事务
        this.stringRedisTemplate.opsForValue().set(RedisConstants.LOGIN_CODE_KEY + phone, code, RedisConstants.LOGIN_CODE_TTL, TimeUnit.SECONDS);
        //5 发送验证码给用户
        log.info("验证码为: {}", code);
        //6 返回发送成功
        return Result.ok();
    }

    /**
     * 完成登录功能
     *
     * @param loginForm
     * @param session
     */
    @Override
    public Result login(LoginFormDTO loginForm, HttpSession session) {

        if (RegexUtils.isPhoneInvalid(loginForm.getPhone())) {
            //校验不通过
            return Result.fail("手机号错误");
        }
        //0 校验验证码格式是否正确
        if (RegexUtils.isCodeInvalid(loginForm.getCode())) {
            //校验失败
            return Result.fail("验证码格式错误");
        }

        //1  TODO -> 从redis中取出验证码
        String rightCode = this.stringRedisTemplate.opsForValue().get(RedisConstants.LOGIN_CODE_KEY + loginForm.getPhone()).toString();
        //2 比对用户发送过来的验证码
        if (rightCode == null || !rightCode.equals(loginForm.getCode())) {
            //3 不匹配 登录失败
            return Result.fail("验证码错误");
        }

        //4 匹配 登陆成功
        //4.1 判断是否是新用户 是 创建一个用户保存到数据库
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getPhone, loginForm.getPhone());
        User user = this.getOne(queryWrapper);
        if (user == null) {
            //用户为空 创建一个用户
            user = creatNewUser(loginForm.getPhone());
            //插入数据库
            this.userMapper.insert(user);
        }

        //TODO 5 -> 保存用户信息到到redis中
        //5.1 随机生成token, 作为登录令牌  不带中划线
        String token = UUID.randomUUID().toString(true);
        //5.2 将User对象转为HashMap存储
        UserDTO userDTO = BeanUtil.copyProperties(user, UserDTO.class);
        Map<String, Object> map = new HashMap<>();
        map.put("id", userDTO.getId().toString());
        map.put("nickName", userDTO.getNickName());
        map.put("icon", userDTO.getIcon());

        //5.3 存储
        this.stringRedisTemplate.opsForHash().putAll(RedisConstants.LOGIN_USER_KEY + token, map);
        //5.4 设置存储有效期
        this.stringRedisTemplate.expire(RedisConstants.LOGIN_USER_KEY + token, RedisConstants.LOGIN_USER_TTL, TimeUnit.MINUTES);
        //6 及那个token返回

        //老用户 直接登录成功 并且 返回token
        return Result.ok(token);
    }

    private User creatNewUser(String Phone) {

        User user = new User();
        user.setPhone(Phone);
        //生成一个随机名字
        user.setNickName(SystemConstants.USER_NICK_NAME_PREFIX + RandomUtil.randomString(10));

        return user;
    }
}
