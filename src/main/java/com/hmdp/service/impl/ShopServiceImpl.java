package com.hmdp.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.hmdp.dto.Result;
import com.hmdp.entity.Shop;
import com.hmdp.mapper.ShopMapper;
import com.hmdp.service.IShopService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.utils.RedisConstants;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
public class ShopServiceImpl extends ServiceImpl<ShopMapper, Shop> implements IShopService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Result queryById(Long id) {
        //1 从redis查询商铺缓存
        String shopJson = this.stringRedisTemplate.opsForValue().get(RedisConstants.CACHE_SHOP_KEY + id);
        //2 判断缓存是否命中
        if (StrUtil.isNotBlank(shopJson)) {
            //3 命中
            //反序列化
            Shop shop = JSONUtil.toBean(shopJson, Shop.class);
            return Result.ok(shop);
        }

        //4 未命中 查询数据库
        Shop shop = this.getById(id);
        //5 判断商铺是否存在
        if (shop != null) {
            //6 存在 将商铺信息存到redis  添加过期时间  30分钟
            this.stringRedisTemplate.opsForValue().set(RedisConstants.CACHE_SHOP_KEY + id, JSONUtil.toJsonStr(shop), RedisConstants.CACHE_SHOP_TTL, TimeUnit.MINUTES);

            return Result.ok(shop);
        }
        //7 返回404
        return Result.fail("商铺信息不存在");
    }

    /**
     * 修改商品信息
     * @param shop
     * @return
     */
    @Override
    @Transactional
    public Result update(Shop shop) {
        if (shop.getId() == null) {
            Result.fail("店铺id不能为空");
        }
        //1 首先更新数据库
        this.updateById(shop);
        //2 删除缓存
        this.stringRedisTemplate.delete(RedisConstants.CACHE_SHOP_KEY + shop.getId());
        //3 返回结果
        return Result.ok();
    }
}
