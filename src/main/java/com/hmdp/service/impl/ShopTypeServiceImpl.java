package com.hmdp.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.hmdp.dto.Result;
import com.hmdp.entity.ShopType;
import com.hmdp.mapper.ShopTypeMapper;
import com.hmdp.service.IShopTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmdp.utils.RedisConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 虎哥
 * @since 2021-12-22
 */
@Service
public class ShopTypeServiceImpl extends ServiceImpl<ShopTypeMapper, ShopType> implements IShopTypeService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;
    /**
     * 查询所有商品类型
     * @return
     */
    @Override
    public Result queryOfShopType() {
        //1 判断缓存中是否存在商品类型数据
        String shopType = this.stringRedisTemplate.opsForValue().get(RedisConstants.SHOP_TYPE_KEY);
        //2 存在 直接返回
        if (StrUtil.isNotBlank(shopType)) {
            List<ShopType> list = JSONUtil.toList(shopType, ShopType.class);
            return Result.ok(list);
        }
        //3 不存在 查询数据库
        List<ShopType> list = this.list();
        if (list == null) {
            return Result.fail("不存在");
        }
        //4 并且存到缓存里面
        this.stringRedisTemplate.opsForValue().set(RedisConstants.SHOP_TYPE_KEY, JSONUtil.toJsonStr(list));

        return Result.ok(list);
    }
}
